﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TapToStart : MonoBehaviour {

	
	public Text text;
	private bool isAnimating = false;
	// Use this for initialization
	private void Awake()
	{
		Application.targetFrameRate = 60;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void Start ()
	{
		Show();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown(0) && !Application.isShowingSplashScreen)
		{
			SceneManager.LoadScene("GamePlay");
		}
		
	}
	public void Show()
	{
		Hashtable tweenParams = new Hashtable
		{
			{"from", text.color},
			{"to", new Color(text.color.r, text.color.g, text.color.b,1)},
			{"time", .35f},
			{"onupdate", "OnColorUpdated"},
			{"oncomplete", "StartAnimation"},
		};

		iTween.ValueTo(gameObject, tweenParams);
	}

	private void StartAnimation()
	{
		Hashtable tweenParams = new Hashtable
		{
			{"from", text.color},
			{"to", new Color(text.color.r, text.color.g, text.color.b,.45f)},
			{"time", .4f},
			{"loopType","pingPong"},
			{"onupdate", "OnColorUpdated"}
		};

		iTween.ValueTo(gameObject, tweenParams);
	}
	
	public void HideText()
	{
		iTween.Stop(gameObject);
		text.color = new Color(text.color.r,text.color.g,text.color.b,0);
	}
	
	private void OnColorUpdated(Color color)
	{
		text.color = color;
	}
	
}
