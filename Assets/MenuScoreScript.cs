﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScoreScript : MonoBehaviour {

	
	// Use this for initialization
	void Start ()
	{
		var highScoreText = GetComponent<Text>();
		highScoreText.text = "Best: " + PlayerPrefs.GetInt("highscore",0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
