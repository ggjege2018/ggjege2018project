﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColorManager : MonoSingleton<ColorManager>
{
	private List<float> availableColors;
	private int currentIndexForColor;
	
	// Use this for initialization
	void Start ()
	{
		availableColors = new List<float>();
		
		var currentHue = Random.RandomRange(0, 360);
		availableColors.Add(currentHue);
		
		var increaseCount = 360f / (GameManager.Instance.Settings.MaxPlayerCount+1);
		for (var i = 0; i < GameManager.Instance.Settings.MaxPlayerCount; i++)
		{
			currentHue = (int)(currentHue + increaseCount) % 360;
			availableColors.Add(currentHue);
		}

		availableColors.Shuffle();

		currentIndexForColor = 1;
	}
	
	public Color GetRandomColor()
	{
		var currentHue = availableColors.ElementAt(currentIndexForColor);
		currentIndexForColor = (currentIndexForColor + 1) % availableColors.Count;
		
		return Color.HSVToRGB(currentHue/360f,GameManager.Instance.Settings.GeneralSaturation,GameManager.Instance.Settings.GeneralValue);
	}

}
