﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RibbonTrail : MonoBehaviour
{
    private List<VertexPair> pairs;

    private Mesh trailMesh;

    [SerializeField]
    private Transform h1, h2, h3;

    public float DistanceThreshold = 0.1f;
    public float DiminishSpeed = 1.0f;

    private Vector3 p1, p2;
    private GameObject ribbon;

    // Use this for initialization
    void Start()
    {
        ribbon = new GameObject("Ribbon", typeof(MeshFilter), typeof(MeshRenderer));
        //ribbon.hideFlags = HideFlags.HideAndDontSave;
        pairs = new List<VertexPair>();
        trailMesh = new Mesh();
        ribbon.GetComponent<MeshFilter>().mesh = trailMesh;
        ribbon.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
    }

    // Update is called once per frame
    void Update()
    {
        ThresholdCheck();
        UpdateTrail();
    }

    private void UpdateTrail()
    {
        for (int i = pairs.Count - 1; i >= 0; i--)
        {
            var pair = pairs[i];
            var center = Vector3.Lerp(pair.a, pair.b, 0.5f);
            pair.a = Vector3.Lerp(pair.a, center, Time.deltaTime * DiminishSpeed);
            pair.b = Vector3.Lerp(pair.b, center, Time.deltaTime * DiminishSpeed);

            if ((pair.a - pair.b).sqrMagnitude < 0.001f)
            {
                pairs.RemoveAt(i);
            }
        }
        SetMesh();
    }

    private void ThresholdCheck()
    {
        float sqrThreshold = DistanceThreshold * DistanceThreshold;
        Vector3 diff1 = h1.position - p1;
        Vector3 diff2 = h2.position - p2;

        if (diff1.sqrMagnitude > sqrThreshold || diff2.sqrMagnitude > sqrThreshold)
        {
            AddTail();
        }
    }

    private void AddTail()
    {
        VertexPair pair = new VertexPair();
        pair.a = p1 = h1.position;
        pair.b = p2 = h2.position;
        pair.normal = h3.position - h1.position;
        pairs.Add(pair);

        SetMesh();
    }

    private void SetMesh()
    {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        int cnt = 0;
        float uvStep = pairs.Count == 0 ? 0 : 1f / pairs.Count;
        foreach (var item in pairs)
        {
            vertices.Add(item.a);
            vertices.Add(item.b);

            uvs.Add(new Vector2(cnt * uvStep, 1));
            uvs.Add(new Vector2(cnt * uvStep, 0));

            normals.Add(item.normal);
            normals.Add(item.normal);

            cnt++;
        }
        List<int> triangles = new List<int>();

        for (int i = 0; i < pairs.Count - 1; i++)
        {
            int a1 = i * 2;
            int a2 = (i + 1) * 2;
            int b1 = a1 + 1;
            int b2 = a2 + 1;

            List<int> tris = new List<int>() { a1, b2, b1, a1, a2, b2 };

            triangles.AddRange(tris);
            tris.Reverse();
            triangles.AddRange(tris);
        }
        trailMesh.Clear();
        trailMesh.vertices = vertices.ToArray();
        trailMesh.triangles = triangles.ToArray();
        trailMesh.uv = uvs.ToArray();
        trailMesh.normals = normals.ToArray();
    }
}
