﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexPair
{
    public Vector3 a, b;
    public Vector3 normal;
}
