﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logo : MonoBehaviour {

	public SpriteRenderer sprite;


	private bool isAnimating = false;

	private int hue = 0;
	// Use this for initialization
	void Start () {
		
		Hashtable tweenParams = new Hashtable
		{
			{"y", transform.position.y - .25},
			{"time", 2f},
			{"looptype","pingPong"},
			{"easetype",iTween.EaseType.easeInOutQuad}
		};

		iTween.MoveTo(gameObject, tweenParams);
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!isAnimating)
		{
			hue = (hue + 2) % 360;

			sprite.color = Color.HSVToRGB(hue/360f, .5f, 1f);

		}
	}

	public void HideText()
	{
		isAnimating = true;
		Hashtable tweenParams = new Hashtable
		{
			{"from", sprite.color},
			{"to", new Color(sprite.color.r, sprite.color.g, sprite.color.b,0)},
			{"time", .2f},
			{"onupdate", "OnColorUpdated"},
			{"oncomplete", "OnColorCompleted"}
		};

		iTween.ValueTo(gameObject, tweenParams);
	}
	
	private void OnColorUpdated(Color color)
	{
		sprite.color = color;
	}
	
	private void OnColorCompleted()
	{
		DestroyImmediate(gameObject);
	}
}
