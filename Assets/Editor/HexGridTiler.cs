﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class HexGridTiler
{
    
    [MenuItem("HexIO Game Utilities/Layout Hexagons MENU")]
    public static void HexTilerForMenu()
    {
        var blockTemplate = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/MenuHex.prefab");

        var blocksObject = GameObject.Find("Blocks");
        if (blocksObject != null)
        {
            GameObject.DestroyImmediate(blocksObject);
        }
        
        blocksObject = new GameObject("Blocks");
        blocksObject.transform.position = Vector3.zero;
        
        var yTileCount = 100;
        var yHalfCount =  (yTileCount / 2f);
        var yOffset = 0;
        var gameAreaOffset = 0;
        
        for (var y = yOffset ; y < yTileCount-yOffset; y++)
        {
            var difference =  yHalfCount - Math.Abs(y - yHalfCount);

            for (var x = 0; x < difference ; x++)
            {
                var block = PrefabUtility.InstantiatePrefab(blockTemplate) as GameObject;
                var hexTile = block.GetComponent<HexTile>();
                
                var position = new Vector3((x-(difference/2f)) * hexTile.Width ,(yHalfCount-y)* hexTile.Height * .77f, 0);
                
                block.name = "Block [" + x + ", " + y + "]";
                block.transform.SetParent(blocksObject.transform);
                block.transform.localPosition = position;
                hexTile.ChangeSprite((x + (difference%3)) % 3 == 0); //1d1c2b
                hexTile.SetColor(new Color32(26, 25, 38, 255));
                 var isInGameArea = difference > yOffset + gameAreaOffset && (difference/2f - (Math.Abs(x-(difference/2f))) > gameAreaOffset);
                
                
                if (!isInGameArea)
                {
                    hexTile.IsOutOfGame = true;
                    hexTile.SetColor(new Color32(220,0,0,255));
                }
                else
                {
                    hexTile.Index = new Vector2(x,y-yOffset);
                    hexTile.IsAvailable = true;
                }
                
               
            }
            
        }
        
    }
    
    [MenuItem("HexIO Game Utilities/Layout Hexagons")]
         public static void HexTiler()
         {
             var blockTemplate = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/hexagonItem.prefab");
     
             var blocksObject = GameObject.Find("Blocks");
             if (blocksObject != null)
             {
                 GameObject.DestroyImmediate(blocksObject);
             }
             
             blocksObject = new GameObject("Blocks");
             blocksObject.transform.position = Vector3.zero;
             
             var yTileCount = 200;
             var yHalfCount =  (yTileCount / 2f);
             var yOffset = 50;
             var gameAreaOffset = 15;
             
             for (var y = yOffset ; y < yTileCount-yOffset; y++)
             {
                 var difference =  yHalfCount - Math.Abs(y - yHalfCount);
     
                 for (var x = 0; x < difference ; x++)
                 {
                     var block = PrefabUtility.InstantiatePrefab(blockTemplate) as GameObject;
                     var hexTile = block.GetComponent<HexTile>();
                     
                     var position = new Vector3((x-(difference/2f)) * hexTile.Width ,(yHalfCount-y)* hexTile.Height * .77f, 0);
                     
                     block.name = "Block [" + x + ", " + y + "]";
                     block.transform.SetParent(blocksObject.transform);
                     block.transform.localPosition = position;
                     hexTile.ChangeSprite((x + (difference%3)) % 3 == 0); //1d1c2b
                     hexTile.SetColor(new Color32(26, 25, 38, 255));
                      var isInGameArea = difference > yOffset + gameAreaOffset && (difference/2f - (Math.Abs(x-(difference/2f))) > gameAreaOffset);
                     
                     
                     if (!isInGameArea)
                     {
                         hexTile.IsOutOfGame = true;
                         hexTile.SetColor(new Color32(220,0,0,255));
                     }
                     else
                     {
                         hexTile.Index = new Vector2(x,y-yOffset);
                         hexTile.IsAvailable = true;
                     }
                     
                    
                 }
                 
             }
             
             blocksObject.transform.rotation = Quaternion.AngleAxis(90,new Vector3(0,0,1));
             
         }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
