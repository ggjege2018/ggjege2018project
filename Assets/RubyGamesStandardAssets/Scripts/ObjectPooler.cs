﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct ObjectPoolItem {
	public GameObject ObjectToPool;
	public GameObject Parent;
	public int AmountToPool;
	public bool ShouldExpand;
}

public class ObjectPooler : MonoSingleton<ObjectPooler> {

	public List<ObjectPoolItem> ItemsToPool;
	public List<GameObject> PooledObjects;

	// Use this for initialization
	void Start ()
	{
		PooledObjects = new List<GameObject>();
		foreach (var item in ItemsToPool) {
			for (var i = 0; i < item.AmountToPool; i++) {
				SpawnObject(item);
			}
		}
	}
	
	public GameObject GetPooledObject(string tag)
	{
		GameObject itemToReturn = null;
		for (var i = 0; i < PooledObjects.Count; i++) {
			if (!PooledObjects[i].activeInHierarchy && PooledObjects[i].CompareTag(tag)) {
				itemToReturn =  PooledObjects[i];
				break;
			}
		}

		if (!itemToReturn)
		{
			foreach (var item in ItemsToPool) {
				if (item.ObjectToPool.CompareTag(tag)) {
					if (item.ShouldExpand)
					{
						itemToReturn = SpawnObject(item);
						break;
					}
				}
			}
		}
		
		itemToReturn.SetActive(true);
		return itemToReturn;
	}

	private GameObject SpawnObject(ObjectPoolItem item)
	{
		var obj = Instantiate(item.ObjectToPool);
		obj.SetActive(false);
		obj.transform.SetParent(item.Parent.transform);
		PooledObjects.Add(obj);
		return obj;
	}
	
}
