﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager
{
    #region Singleton Implementation

    protected static TimeManager _instance;
    protected static TimeManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TimeManager();
            }
            return _instance;
        }
    }
    protected TimeManager() { }

    #endregion

    #region Time Multipliers

    public static float GeneralTimeMultiplier = 1.0f;
    public static float EnemyTimeMultiplier = 1.0f;
    public static float PlayerTimeMultiplier = 1.0f;

    #endregion

    public static float DeltaTime
    {
        get
        {
            return Time.deltaTime * GeneralTimeMultiplier;
        }
    }

    public static float PlayerDeltaTime
    {
        get
        {
            return DeltaTime * PlayerTimeMultiplier;
        }
    }

    public static float EnemyDeltaTime
    {
        get
        {
            return DeltaTime * EnemyTimeMultiplier;
        }
    }
}
