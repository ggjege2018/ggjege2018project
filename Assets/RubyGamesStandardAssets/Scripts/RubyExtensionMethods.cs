﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RubyExtensionMethods
{
    #region Transform Extensions

    public static void SetX(this Transform transform, float x)
    {
        Vector3 pos = transform.position;
        pos.x = x;
        transform.position = pos;
    }

    public static void SetY(this Transform transform, float y)
    {
        Vector3 pos = transform.position;
        pos.y = y;
        transform.position = pos;
    }

    public static void SetZ(this Transform transform, float z)
    {
        Vector3 pos = transform.position;
        pos.z = z;
        transform.position = pos;
    }

    public static void SetXY(this Transform transform, Vector2 xy)
    {
        transform.SetXY(xy.x, xy.y);
    }

    public static void SetXY(this Transform transform, float x, float y)
    {
        Vector3 pos = transform.position;
        pos.x = x;
        pos.y = y;
        transform.position = pos;
    }

    public static void SetXZ(this Transform transform, Vector2 xz)
    {
        transform.SetXZ(xz.x, xz.y);
    }

    public static void SetXZ(this Transform transform, float x, float z)
    {
        Vector3 pos = transform.position;
        pos.x = x;
        pos.z = z;
        transform.position = pos;
    }

    #endregion

    #region Vector3 Extensions

    public static Vector2 xy(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static Vector2 xz(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    #endregion

    public static T GetRandom<T>(this List<T> list)
    {
        T result = default(T);

        int rndIndex = Random.Range(0, list.Count);
        result = list[rndIndex];

        return result;
    }

    public static T GetRandom<T>(this T[] list)
    {
        T result = default(T);

        int rndIndex = Random.Range(0, list.Length);
        result = list[rndIndex];

        return result;
    }
    
    public static void Shuffle<T>(this IList<T> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = Random.Range(0,n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }
}
