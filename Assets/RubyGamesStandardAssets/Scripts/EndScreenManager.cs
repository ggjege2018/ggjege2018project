﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class EndScreenManager : MonoSingleton<EndScreenManager>
{
    public float FadeInTime = 0.5f;
    public float FadeOutTime = 0.5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator FadeIn()
    {
        CanvasGroup cg = GetComponent<CanvasGroup>();

        float timer = 0.0f;
        while (timer < FadeInTime)
        {
            timer += TimeManager.DeltaTime;
            float t = timer / FadeInTime;
            cg.alpha = Mathf.Lerp(0f, 1f, t);
            yield return null;
        }

        cg.alpha = 1f;
        cg.interactable = true;
    }

    private IEnumerator FadeOut()
    {
        CanvasGroup cg = GetComponent<CanvasGroup>();

        float timer = 0.0f;
        while (timer < FadeOutTime)
        {
            timer += TimeManager.DeltaTime;
            float t = timer / FadeInTime;
            cg.alpha = Mathf.Lerp(1f, 0f, t);
            yield return null;
        }

        cg.alpha = 0f;
        cg.interactable = false;
    }

    public static void Show()
    {
        if (Instance != null)
        {
            Instance.StartCoroutine(Instance.FadeIn());
        }
        else
        {
            Debug.LogWarning("EndScreenManager is not ready yet...");
        }
    }
}
