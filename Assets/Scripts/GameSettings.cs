﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GameSettings : ScriptableObject {
   
    
    [Header("Debug")]
    public bool DebugMode;

    public int MaxCollectableCount = 300;

    public int MinCollectableCount  = 80;
    
    public int InitialSpawnMaxCount;
    
    public int InitialSpawnMinCount;

    public int MillisecondsToSpawnCollectable  = 1000;

    public int MaxPlayerCount = 10;
    
    [Range(0f, 1f)]
    public float GeneralSaturation  = .8f;
    [Range(0f, 1f)]
    public float GeneralValue  = .6f;
}