﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Application = UnityEngine.Application;

public class GameManager : MonoSingleton<GameManager>
{
    public Transform Blocks;
    public Point PointTemplate;
    public Base BaseTemplate;
    // Use this for initialization
    
    public GameSettings Settings;

    public List<EnemyAI> AIList;
    
    void Start()
    {
        var initalValue = Camera.main.orthographicSize;
        Camera.main.orthographicSize = initalValue*.4f;
        
        iTween.ValueTo(gameObject, new Hashtable
        {
            {"from",Camera.main.orthographicSize},
            {"to",initalValue},
            {"time", 1f},
            {"delay",.1f},
            {"easetype",iTween.EaseType.easeInSine},
            {"onupdate", "OnValueUpdated"},
            {"oncomplete", "OnValueCompleted"}
        });


        StartCoroutine(PlayerSpawner());
    }

    private void OnValueUpdated(float value)
    {
        Camera.main.orthographicSize = value;
    }
    
    private void OnValueCompleted()
    {
        
    }
    
    private void Awake()
    {
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }


    IEnumerator PlayerSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(0+Random.RandomRange(0,10));

            SpawnEnemy();

        }
        
    }

    private void SpawnEnemy()
    {
        if(AIList.Count >= Settings.MaxPlayerCount ) return;
        
        var enemyAI = ObjectPooler.Instance.GetPooledObject("Enemy").GetComponent<EnemyAI>();
        enemyAI.gameObject.SetActive(true);
        
        AIList.Add(enemyAI);
    }
    
    
    
}
