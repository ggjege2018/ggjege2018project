﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public float FollowSpeed = 1.0f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null)
        {
            transform.SetXY(Vector2.Lerp(transform.position.xy(), Target.position.xy(), TimeManager.PlayerDeltaTime * FollowSpeed));
        }
    }
}
