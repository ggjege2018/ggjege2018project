﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoSingleton<CollectibleManager>
{
	
	private double lastSpawnedTime;

	[HideInInspector]
	public List<Point> CollectableList { get; set; }
	
	// Use this for initialization
	void Start () {
		
		CollectableList = new List<Point>();
		
		var initialSpawnCount = Random.Range(GameManager.Instance.Settings.InitialSpawnMinCount, GameManager.Instance.Settings.InitialSpawnMaxCount);
		for (var i = 0; i < initialSpawnCount; i++)
		{
			SpawnPointAt(TileManager.GetRandomAvailableTile(),false);
		}

		lastSpawnedTime = Time.time * 1000;

	}

	private void Update()
	{
		var currentTime = Time.time*1000;
		if (currentTime - lastSpawnedTime > GameManager.Instance.Settings.MillisecondsToSpawnCollectable && CollectableList.Count < GameManager.Instance.Settings.MaxCollectableCount)
		{
			lastSpawnedTime = currentTime;
			SpawnPointAt(TileManager.GetRandomAvailableTile(),true);
			
		}
	}
	

	private void SpawnPointAt(HexTile tile,bool withFade)
	{
		var collectable = ObjectPooler.Instance.GetPooledObject("Point").GetComponent<Point>();
		
		
		var randomUnit = Random.insideUnitCircle*.5f;
		collectable.transform.position = tile.transform.position + 
		                                 new Vector3(randomUnit.x*tile.Width,randomUnit.y*tile.Height,1) ;
		
		CollectableList.Add(collectable);
		
		collectable.gameObject.SetActive(true);
		collectable.Initialize();
		
		collectable.FadeIn(withFade ? .25f : -1f);
		
	}

	public void CollectItem(Point collectable)
	{
		collectable.Destroy();
		CollectableList.Remove(collectable);
	}
}
