﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour
{
    public HexTile CenterTile;
    public int Resources;
    public BaseState State;
    public bool IsPlayerInside = false;
    public bool IsUnderAttack = false;
    
    public int Radius;

    [SerializeField]
    private ScoreScript score;
    private List<HexTile> currentSurrendingTiles;
    
    [HideInInspector]
    public Color AreaColor;
    
    [HideInInspector]
    public Color BaseColor;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    public BaseCharacter Character;
    
    
    
    public enum BaseState
    {
        Small,
        Medium,
        High
    }

    public int PowerVisible = 1;
    public int Power
    {
        get
        {
            return PowerVisible;
        }
        set
        {
            PowerVisible = value;
            UpdateValues();
        }
    }
    private void UpdateValues()
    {
        score.Score = Power;
    }
    
    // Use this for initialization
    void Start()
    {
        spriteRenderer.color = BaseColor;
        
        currentSurrendingTiles  = new List<HexTile>();
        var surroundingTiles = TileManager.GetSurroundingHexTiles(CenterTile, 2);

        foreach (var item in surroundingTiles)
        {
            item.SetColor(AreaColor);
            item.IsAvailable = false;
            currentSurrendingTiles.Add(item);
        }
        
    }
    
}
