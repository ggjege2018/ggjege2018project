﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class EnemyAI : BaseCharacter
{
    [Header("Point Detected Settings")]
    public Point TargetPoint;


    private IEnumerator AIStatePointDetect()
    {
        while (State == AIStates.PointDetect)
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.velocity = (TargetPoint.transform.position - transform.position).xy().normalized * MovementSpeed;
            if (!TargetPoint.gameObject.activeInHierarchy)
            {
                State = AIStates.Exploration;
                break;
            }
            yield return null;
        }
        HandleStates();
    }
}
