﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class EnemyAI : BaseCharacter
{
    public enum AIStates
    {
        Inactive,
        Exploration,
        PointDetect,
        BaseDetectPlayerAtBase,
        BaseDetectEmptyBase,
        BaseUnderAttack,
        ReturnToBase
    }

    public AIStates State;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        HandleStates();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    private void HandleStates()
    {
        switch (State)
        {
            case AIStates.Inactive:
                break;
            case AIStates.Exploration:
                StartCoroutine(AIStateExploration());
                break;
            case AIStates.PointDetect:
                StartCoroutine(AIStatePointDetect());
                break;
            case AIStates.BaseDetectPlayerAtBase:
                StartCoroutine(AIStateBaseDetectPlayerAtBase());
                break;
            case AIStates.BaseDetectEmptyBase:
                StartCoroutine(AIStateBaseDetectEmptyBase());
                break;
            case AIStates.BaseUnderAttack:
                StartCoroutine(AIStateBaseUnderAttack());
                break;
            case AIStates.ReturnToBase:
                StartCoroutine(AIStateReturnToBase());
                break;
            default:
                break;
        }
    }
}
