﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class EnemyAI : BaseCharacter
{
    private IEnumerator AIStateReturnToBase()
    {
        while (State == AIStates.ReturnToBase)
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.velocity = (Base.transform.position - transform.position).xy().normalized * MovementSpeed;
            if (IsBaseReached())
            {
                while (Power > 0)
                {
                    rb.velocity = Vector2.Lerp(rb.velocity, Vector2.zero, TimeManager.PlayerDeltaTime);
                    yield return null;
                }
                State = AIStates.Exploration;
                break;
            }
            yield return null;
        }
        HandleStates();
    }

    private bool IsBaseReached()
    {
        return activityState == ActivityState.Transmission;
    }
}
