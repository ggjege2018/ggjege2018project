﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class EnemyAI : BaseCharacter
{
    [Header("Exploration Settings")]
    [Space()]
    public float TileDeterminationRadius = 3.0f;
    public float PointSenseRadius = 3.0f;
    public HexTile TargetTile;
    public bool isBumpedIntoAreaCollider = false;

    private IEnumerator AIStateExploration()
    {
        DetermineTargetTile();
        CheckLoad();
        float timer = 1f;
        while (State == AIStates.Exploration)
        {
            timer += TimeManager.EnemyDeltaTime;
            if (timer > 1f)
            {
                timer = 0f;
                SensePoints();
            }
            var rb = GetComponent<Rigidbody2D>();
            rb.velocity = (TargetTile.transform.position - transform.position).xy().normalized * MovementSpeed;
            if ((transform.position - TargetTile.transform.position).sqrMagnitude < 0.01f)
            {
                break;
            }
            yield return null;
        }
        HandleStates();
    }

    private void CheckLoad()
    {
        if (Power > 15)
        {
            State = AIStates.ReturnToBase;
        }
    }

    protected new void OnTriggerEnter2D(Collider2D collision)
    {
        if (State == AIStates.Exploration)
        {
            if (collision.tag == "AreaCollider")
            {
                isBumpedIntoAreaCollider = true;
                StartCoroutine(UnstuckFromWall());
            } 
        }
        base.OnTriggerEnter2D(collision);
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        if (State == AIStates.Exploration)
        {
            if (collision.tag == "AreaCollider")
            {
                isBumpedIntoAreaCollider = false;
            } 
        }
        base.OnTriggerExit2D(collision);
    }

    private IEnumerator UnstuckFromWall()
    {
        while (isBumpedIntoAreaCollider)
        {
            Debug.Log("Fixing wall stuck...");
            DetermineTargetTile();
            yield return new WaitForSeconds(1f);
        }
    }

    private void SensePoints()
    {
        var nearbyPoint = Physics2D.OverlapCircle(transform.position.xy(), PointSenseRadius, LayerMask.GetMask("Point"));
        if (nearbyPoint != null)
        {
            TargetPoint = nearbyPoint.GetComponent<Point>();
            State = AIStates.PointDetect;
        }
    }

    private void DetermineTargetTile()
    {
        var nearbyTiles = Physics2D.OverlapCircleAll(transform.position.xy(), TileDeterminationRadius, LayerMask.GetMask("Tile"));
        var nearbyAvailableTiles = nearbyTiles.Where(o => o.GetComponent<HexTile>().IsAvailable).ToList();
        TargetTile = nearbyAvailableTiles.GetRandom().GetComponent<HexTile>();
    }
}
