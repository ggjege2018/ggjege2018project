﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileManager : MonoSingleton<TileManager> {

	public Transform Blocks;
	
	private HexTile[][] hexMap;
	private List<HexTile> hexList;
	private static int size = 150;
	
	private void Awake()
	{
		hexMap = new HexTile[size][];
		
		for (var i = 0; i < size; i++)
		{
			hexMap[i] = new HexTile[size];
			for (var j = 0; j < size; j++)
			{
				hexMap[i][j] = null;
			}
		}
		
		for (var i = 0; i < Blocks.childCount; i++)
		{
			var hex = Blocks.GetChild(i).gameObject.GetComponent<HexTile>();
			
			if(hex.IsOutOfGame) continue;
			
			hexMap[(int)hex.Index.x][(int)hex.Index.y] = hex;
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static HexTile GetTileWithIndex(int xIndex,int yIndex)
	{
		return Instance.hexMap[xIndex][yIndex];
	}

	public static List<HexTile> GetAvailableHexTiles()
	{
		return GetAreaHexTiles().Where(p => p.IsAvailable).ToList();
	}

	public static List<HexTile> GetAreaHexTiles()
	{
		if (Instance.hexList != null) return Instance.hexList;
		
		Instance.hexList = new List<HexTile>();
		
		for (var i = 0; i < size; i++)
		{
			for (var j = 0; j < size; j++)
			{
				if (Instance.hexMap[i][j] != null)
				{
					Instance.hexList.Add(Instance.hexMap[i][j]);
				}
			}
		}

		return Instance.hexList;
	}


	public static List<HexTile> GetSurroundingHexTiles (HexTile root,int radius,List<HexTile> surroundingTiles = null) {
		
		surroundingTiles = surroundingTiles ?? new List<HexTile>();
		
		surroundingTiles.Add(root);
		
		var xIndex = (int)root.Index.x;
		var yIndex = (int)root.Index.y;

		AddItemToTile(Instance.hexMap[xIndex - 1][yIndex], surroundingTiles,radius);
		AddItemToTile(Instance.hexMap[xIndex+1][yIndex],surroundingTiles,radius);
		AddItemToTile(Instance.hexMap[xIndex][yIndex+1],surroundingTiles,radius);
		AddItemToTile(Instance.hexMap[xIndex][yIndex-1],surroundingTiles,radius);

		if (yIndex > 50)
		{
			AddItemToTile(Instance.hexMap[xIndex+1][yIndex-1],surroundingTiles,radius);
			AddItemToTile(Instance.hexMap[xIndex-1][yIndex+1],surroundingTiles,radius);
		}
		else
		{
			AddItemToTile(Instance.hexMap[xIndex+1][yIndex+1],surroundingTiles,radius);
			AddItemToTile(Instance.hexMap[xIndex-1][yIndex-1],surroundingTiles,radius);
		}
		
		
		return surroundingTiles;

	}

	private static void AddItemToTile(HexTile tile, List<HexTile> tileList,int radius)
	{
		if(tile == null) return;
		
		if (radius > 1)
		{
			GetSurroundingHexTiles(tile, radius - 1, tileList);	
		}
		else
		{
			if(!tileList.Contains(tile))
			{
				tileList.Add(tile);	
			}
			
		}
	}
	
	public static HexTile GetRandomAvailableTile(int offset = 0)
	{
		var availableTile = GetAvailableHexTiles().GetRandom();

		if (Mathf.Abs(availableTile.Index.y - 50) < offset)
		{
			return GetRandomAvailableTile(offset);
		}
		
		return availableTile;
	}
}
