﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

	private int score;
	public Text text;
	public int Score
	{
		
		get { return score; }
		set
		{
			score = value;
			text.text = score.ToString();

			if (value <= 0) return;
			
			transform.localScale = Vector3.one*1.2f;
			
			iTween.ScaleTo(gameObject, new Hashtable
			{
				{"x", 1},
				{"y", 1},
				{"time", .3f},
				{"easetype",iTween.EaseType.easeOutSine}
			});
		}
	}
	
	private void Start ()
	{
		
		Score = 0;

	}

	public void UpdateColor(Color32 color)
	{
		text.color = color;
		
	}
	
}