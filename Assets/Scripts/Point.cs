﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    private bool isAnimating;
    private bool isOnArea;
    
    public float ColorChangeSpeed = 1.0f;
    public float radius = 1.0f;
    private SpriteRenderer sr;
    private SpriteRenderer SR
    {
        get { return sr == null ? sr = GetComponent<SpriteRenderer>() : sr; }
    }
    // Use this for initialization
    void Start()
    {
       
    }

    public void Initialize()
    {
        
        float h, s, v;
        h = s = v = 0;
        Color.RGBToHSV(SR.color, out h, out s, out v);
        h = Random.RandomRange(0, 360) / 360f;
        
        SR.color = Color.HSVToRGB(h, s, v);
        
        isOnArea = false;
        isAnimating = false;
        OnColorUpdated(0f);
        
        StartCoroutine(ColorChange());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator ColorChange()
    {
        while (true)
        {
            if (isAnimating || !isOnArea || !gameObject.activeInHierarchy)
            {
                yield return new WaitForSeconds(.25f);
                continue;
            }
            
            float h, s, v;
            h = s = v = 0;
            Color.RGBToHSV(SR.color, out h, out s, out v);

            h += TimeManager.DeltaTime * ColorChangeSpeed;

            h -= h > 1f ? 1 : 0;

            SR.color = Color.HSVToRGB(h, s, v);

            yield return null; 
        }
    }

    public void FadeIn(float duration = .25f)
    {
        if (duration == -1f)
        {
            OnColorUpdated(1f);
            return;
        }
        
        iTween.Stop(gameObject);
        isAnimating = true;
        
        Hashtable tweenParams = new Hashtable
        {
            {"from", 0f},
            {"to", 1f},
            {"time", .25f},
            {"onupdate", "OnColorUpdated"},
            {"oncomplete", "OnFadeInCompleted"}
        };

        iTween.ValueTo(gameObject, tweenParams);
    }
    
    public void FadeOut()
    {
        iTween.Stop(gameObject);
        
        isAnimating = true;
        Hashtable tweenParams = new Hashtable
        {
            {"from", SR.color.a},
            {"to", 0f},
            {"time", .25f},
            {"onupdate", "OnColorUpdated"},
            {"oncomplete", "OnFadeOutCompleted"}
        };

        iTween.ValueTo(gameObject, tweenParams);
    }

    private void OnColorUpdated(float value)
    {
        SR.color = new Color(SR.color.r,SR.color.g,SR.color.b,value);
    }
    
    private void OnFadeInCompleted()
    {
        isAnimating = false;
        isOnArea = true;
    }
    
    private void OnFadeOutCompleted()
    {
        isOnArea = false;
        isAnimating = false;
        gameObject.SetActive(false);
        
    }

    public void Destroy()
    {
        isOnArea = false;
        isAnimating = false;
        gameObject.SetActive(false);
        OnColorUpdated(0);
    }

}
