﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile : MonoBehaviour
{

	public Sprite BrightSprite;
	public Sprite DarkSprite;
	
    [SerializeField]
	private SpriteRenderer spriteRenderer;

    public float FlipTime = 0.25f;

	public float Width
	{
		get { return spriteRenderer.bounds.size.x; }
	}

	public float Height
	{
		get { return spriteRenderer.bounds.size.y; }
	}
	
	public Color Color
	{
		get { return spriteRenderer.color; }
	}

	private bool isOutOfGame;

	public bool IsOutOfGame
	{
		get { return isOutOfGame; }
		set { isOutOfGame = value; }
	}
	
    [SerializeField]
	private bool isAvailable;

	public bool IsAvailable
	{
		get { return isAvailable; }
		set { isAvailable = value; }
	}

	//private Vector2 index;
	public Vector2 Index;
	// Use this for initialization
	private void Awake()
	{
	}
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	// Update is called once per frame
	public void SetColor (Color32 color)
	{
		spriteRenderer.color = color;
	}

    public void SetColorFlip(Color32 color)
    {
        StartCoroutine(FlipColor(color));
    }

	public void ChangeSprite(bool isDark)
	{
		if (isDark)
		{
			spriteRenderer.sprite = DarkSprite;
		}
		else
		{
			spriteRenderer.sprite = BrightSprite;
		}
	}

	private IEnumerator FlipColor(Color32 color)
    {
        yield return new WaitForSeconds(5);
        float timer = 0.0f;
        while ((timer += TimeManager.DeltaTime) <= FlipTime / 2f )
        {
            Vector3 euler = Vector3.zero;
            euler.y = Mathf.Lerp(0, 90, timer / (FlipTime / 2f));
            transform.localRotation = Quaternion.Euler(euler);
            yield return null;
        }

        timer = 0.0f;
        spriteRenderer.color = color;

        while ((timer += TimeManager.DeltaTime) <= FlipTime / 2f)
        {
            Vector3 euler = Vector3.zero;
            euler.y = Mathf.Lerp(90, 180, timer / (FlipTime / 2f));
            transform.localRotation = Quaternion.Euler(euler);
            yield return null;
        }
    }
}
