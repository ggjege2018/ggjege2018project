﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Player : BaseCharacter
{
   
    // Use this for initialization
    void Start()
    {
        base.Start();
    }

    void Update()
    {
        base.Update();
    }
    
    // Update is called once per frame
    protected override void ControlCharacter()
    {
        float hor = CrossPlatformInputManager.GetAxis("Horizontal");
        float ver = CrossPlatformInputManager.GetAxis("Vertical");

        GetComponent<Rigidbody2D>().velocity = new Vector2(hor, ver) * MovementSpeed;

        //transform.Translate(new Vector2(hor, ver) * MovementSpeed * TimeManager.PlayerDeltaTime);
        base.ControlCharacter();
    }

   
}
