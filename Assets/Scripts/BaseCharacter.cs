﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActivityState
{
    None = 1,
    Transmission = 2,
    Receive = 3
}
    

public class BaseCharacter : MonoBehaviour
{
    [Header("Character General Settings")]
    [Space()]
    public float MovementSpeed = 1.0f;
    public float radius = 1.0f;
    public int PowerVisible = 1;
    public int Power
    {
        get
        {
            return PowerVisible;
        }
        set
        {
            PowerVisible = value;
            UpdateValues();
        }
    }

    public ScoreScript score;
    [HideInInspector]
    public Base Base;
    [HideInInspector]
    private Base LastTouchedBase;
    public ActivityState activityState;
    
    [HideInInspector]
    public Color Color;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    private bool activateTransmission;
    private bool activateReceiver;
    
    
    private void UpdateValues()
    {
        bool isIncrease = score.Score < Power;
        score.Score = Power;
        if (isIncrease)
        {
            transform.localScale += Vector3.one * 0.004f;
            MovementSpeed = Mathf.Max(MovementSpeed - 0.01f, 0.2f);
        }
        else
        {
            transform.localScale -= Vector3.one * 0.004f;
            MovementSpeed = Mathf.Min(MovementSpeed + 0.01f, 2f);
        }
       
    }

   

    protected virtual void ControlCharacter()
    {

    }

    // Use this for initialization
    protected void Start()
    {
        Color = ColorManager.Instance.GetRandomColor();
        spriteRenderer.color = Color;
        SpawnBase();
        
        transform.position = Base.transform.position + (Vector3.up*1f);

        activityState = ActivityState.None;
        LastTouchedBase = null;
        Power = 0;
        Base.Power = 0;
    }

    private float actionTime;
    // Update is called once per frame
    protected void Update()
    {
        ControlCharacter();

        actionTime += TimeManager.PlayerDeltaTime*100;
        if (actionTime > 8f)
        {
            DoAction();
            actionTime = 0;
        }
    }

    void DoAction()
    {
        if(!LastTouchedBase) return;
        
        if (activityState == ActivityState.Receive && LastTouchedBase.Power > 0)
        {
            Power++;
            LastTouchedBase.Power--;

        }
        else if(activityState == ActivityState.Transmission && Power>0)
        {
            Power--;
            LastTouchedBase.Power++;
        }
    }

    void SpawnBase()
    {
        var baseItem = ObjectPooler.Instance.GetPooledObject("Base").GetComponent<Base>();
        var tile = TileManager.GetRandomAvailableTile(6);
        baseItem.CenterTile = tile;
        baseItem.transform.position = tile.transform.position;
        baseItem.AreaColor = Color * .9f;
        baseItem.BaseColor = Color * .8f;
        tile.IsAvailable = false;
        baseItem.Character = this;
        
        baseItem.gameObject.SetActive(true);

        Base = baseItem;
    }
    
    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Point")
        {
            var pointObject = other.gameObject.GetComponent<Point>();
            CollectibleManager.Instance.CollectItem(pointObject);
            Power++; 
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Base")
        {
            var baseObject = other.gameObject.GetComponent<Base>();

            if (baseObject == Base)
            {
                activityState = ActivityState.Transmission;
            }
            else
            {
                activityState = ActivityState.Receive;
            }
            LastTouchedBase = baseObject;
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Base")
        {
            activityState = ActivityState.None;
            LastTouchedBase = null;
        }
    }
}
