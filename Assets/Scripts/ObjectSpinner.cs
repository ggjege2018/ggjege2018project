﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpinner : MonoBehaviour
{
    public Vector3 Direction;
    public float Speed;
    public Space Space;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Direction, Speed * Time.deltaTime * 360f, Space);
    }
}
