using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace UnityStandardAssets.CrossPlatformInput
{
	public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}

        public Image JoystickHead;
        private bool isJoystickActive = false;
         
		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

		Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

		void OnEnable()
		{
			CreateVirtualAxes();
		}

        void Start()
        {
            m_StartPos = transform.position;
        }

		private float xVel = 0;
		private float yVel = 0;
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                isJoystickActive = true;
                HandleInput();
            }

            if (!Input.GetMouseButton(0) && isJoystickActive)
            {
                isJoystickActive = false;
	            yVel = previousDelta.y;
	            xVel = -previousDelta.x;

            }

            if (isJoystickActive)
            {
                SimulateJoystick(Input.mousePosition);
            }
            else
            {
	            xVel *= .95f;
	            yVel *= .95f;

	            if (Math.Abs(xVel) < 0.01f)
	            {
		            xVel = 0;
	            }
	            
	            if (Math.Abs(yVel) < 0.01f)
	            {
		            yVel = 0;
	            }
	            
	            m_HorizontalVirtualAxis.Update(xVel);
	            m_VerticalVirtualAxis.Update(yVel);
            }

            GetComponent<Image>().enabled = JoystickHead.enabled = isJoystickActive;
        }

        private void SimulateJoystick(Vector3 mousePosition)
        {
            Vector3 newPos = Vector3.zero;

            if (m_UseX)
            {
                int delta = (int)(mousePosition.x - m_StartPos.x);
                delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
                newPos.x = delta;
            }

            if (m_UseY)
            {
                int delta = (int)(mousePosition.y - m_StartPos.y);
                delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
                newPos.y = delta;
            }
            newPos = newPos.sqrMagnitude > MovementRange * MovementRange ? newPos.normalized * MovementRange : newPos;
            JoystickHead.transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
            UpdateVirtualAxes(JoystickHead.transform.position);
        }

        private void HandleInput()
        {
            var rt = GetComponent<RectTransform>();
            m_StartPos = rt.position = JoystickHead.transform.position = Input.mousePosition;

        }


		private Vector3 previousDelta;
        void UpdateVirtualAxes(Vector3 value)
		{
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(-delta.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(delta.y);
			}

			previousDelta = delta;
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		//public void OnDrag(PointerEventData data)
		//{
		//	Vector3 newPos = Vector3.zero;
        //
		//	if (m_UseX)
		//	{
		//		int delta = (int)(data.position.x - m_StartPos.x);
		//		delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
		//		newPos.x = delta;
		//	}
        //
		//	if (m_UseY)
		//	{
		//		int delta = (int)(data.position.y - m_StartPos.y);
		//		delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
		//		newPos.y = delta;
		//	}
		//	transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
		//	UpdateVirtualAxes(transform.position);
		//}


		public void OnPointerUp(PointerEventData data)
		{
			transform.position = m_StartPos;
			UpdateVirtualAxes(m_StartPos);
		}


		public void OnPointerDown(PointerEventData data) { }

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}
	}
}