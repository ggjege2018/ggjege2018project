﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksMovement : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{

		var item = transform.GetChild(0).GetComponent<HexTile>();
		
		iTween.MoveBy(gameObject, iTween.Hash("y", item.Height*3,"x",item.Width*3,"easetype", iTween.EaseType.linear, "loopType", "loop"));
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
